#!/bin/bash

# Install dependencies only for Docker.
[[ ! -e  /.dockerenv ]] && [[ ! -e /.dockerinit ]] && exit 0
set -xe

# Copy over testing configuration.
cp .env.testing.gitlab .env
