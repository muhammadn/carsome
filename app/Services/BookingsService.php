<?php

namespace App\Services;

use App\Repositories\BookingRepository;
use App\Transformer\BookingTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use Timeslot\Timeslot;
use Timeslot\TimeslotCollection;
use Carbon\Carbon;

class BookingsService
{
    // dependency injection to make code easier to test
    // use Repository pattern to make code more easier to be managed
    private $bookingRepo;

    public function __construct(BookingRepository $bookingRepo)
    {
        $this->bookingRepository = $bookingRepo;
    }

    public function all()
    {

        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer());

        // Call the data provider repo
        $bookings = $this->bookingRepository->all();

        // Make a resource out of the data and
        $resource = new Collection($bookings, new BookingTransformer(), 'bookings');

        // Run all transformers
        return $manager->createData($resource)->toArray();
    }
}
