<?php

namespace App\Services;

use App\Repositories\BookingRepository;
use App\Services\OpeningHoursService;
use Carbon\Carbon;
use JWTAuth;
use Timeslot\Timeslot;
use Timeslot\TimeslotCollection;

class BookingsSelection
{
    // dependency injection to make code easier to test
    // use Repository pattern to make code more easier to be managed
    private $bookingRepo;

    public function __construct(BookingRepository $bookingRepo)
    {
        $this->bookingRepository = $bookingRepo;
    }
    
    // private function since we do not want to expose outside of this class
    private function checkOpen($datetime)
    {
        $hours = new OpeningHoursService;         
        $hours = $hours->check();
        $opens = $hours->isOpenAt($datetime); // check check if it is open at the time
        return $opens;
    }

    public function checkSlot($input)
    {
        $datetime = Carbon::parse($input->slot);
        $open = $this->checkOpen($datetime); // check if we're open at this time

	// Check user to only book once every 3 weeks
        $slotEligibility = $this->checkSlotWeeks();
        // check if there is any bookings and enforce user can book 3 weeks from last boking
        if (!$slotEligibility)
            return (object) ['success' => false,
                             'message' => 'Sorry, you can only book once every 3 weeks'];

        if ($open) {
            // Check if slots available
            $inspectionSlot =$this->inspectionSlots($datetime);
	    if (!$inspectionSlot)
		    return (object) ['success' => false,
                                     'message' => 'Sorry, this timeslot is full'];

            $start = $datetime->copy()->startOfHour(); // set to the start of our acordingly
            $end = $start->copy()->addHour(); // add 1 hour

            $findBookedSlots = $this->bookingRepository->betweenHours($start, $end);
            // use guard clauses instead of "if else" so to make code lesser and more readable
            // if there are no slots between the hour, user can create booking
	    $user = JWTAuth::parseToken()->toUser();
            if ($user->bookings()->whereBetween('slot', [$start, $end])->get()->isEmpty())
                return $this->bookingRepository->create(
                    ['slot' => $datetime,
                     'user_id' => $user->id]
                );

            // return user cannot create booking because it's the same booking hour
            return (object) ['success' => false,
                        'message' => 'Sorry, you cannot book on the same hour'];
        } 

        return (object) ['success' => false,
                  'message' => 'Carsome is not opened on this date or time'];
    }

    // check slot if there are within 3 weeks interval
    private function checkSlotWeeks()
    {
        $user = JWTAuth::parseToken()->toUser();
        // get the latest booking
        $booking = $user->bookings()->latest('slot')->first();
        if (!isset($booking) || $booking->slot > Carbon::parse(
          $booking
            ->slot
          )->addWeeks(3)
            // user has only booked after 3 weeks
        ) return true;
        // user cannot book because it's not yet 3 weeks
        return false;
    }

    // logic for inspection slots
    public function inspectionSlots($datetime)
    {
        $timeslot = new Timeslot($datetime->toDateString() . ' 09:00:00', 0, 30);
	// 30 minutes slot of the whole day
        $collection = TimeslotCollection::create($timeslot, 18);

	$schedules = [];
	// getIterator() is an undocumented timeslot library method
	foreach($collection->getIterator() as $key => $collect){
            $schedules[$key]['start'] = $collect->start();
	    $schedules[$key]['end'] = $collect->end();
        }

	// find the start and end date/time
        $schedule = array_filter($schedules, function($schedule) use($datetime) {
                       return $schedule['start'] <= $datetime  && $schedule['end'] >= $datetime;
                    });

	// reset the array pointer index
	$start = reset($schedule)['start'];
        $end = reset($schedule)['end'];

	$bookedSlotsCount = $this
            ->bookingRepository
	    ->betweenHours($start, $end)
            ->count();

	// if the day is saturday, only 4 slots available
	if ($datetime->dayOfWeek == 6) {
           if ($bookedSlotsCount >= 4)
               return false;
        }

	// if day is any other day, 2 slots maximum
	if ($bookedSlotsCount >= 2)
	    return false;

        // All is okay to add for this slot	
        return true;
    }
}
