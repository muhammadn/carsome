<?php

namespace App\Services;

use Spatie\OpeningHours\OpeningHours;
use Carbon\Carbon;

class OpeningHoursService
{

    public function check()
    {
        $openingHours = [
          (object) ['day' => 'Mon',
           'open_at' => '9:00',
           'close_at' => '18:00'],
          (object) ['day' => 'Tue',
           'open_at' => '9:00',
           'close_at' => '18:00'],
          (object) ['day' => 'Wed',
           'open_at' => '9:00',
           'close_at' => '18:00'],
          (object) ['day' => 'Thu',
           'open_at' => '9:00',
           'close_at' => '18:00'],
          (object) ['day' => 'Fri',
           'open_at' => '9:00',
           'close_at' => '18:00'],
          (object) ['day' => 'Sat',
           'open_at' => '9:00',
           'close_at' => '18:00'],
        ]; // Sun is excluded because it is not open

        $hours = [];
        foreach($openingHours as $openingHour) {
            switch($openingHour->day){
            case "Mon":
                $time = Carbon::parse($openingHour->open_at)->format('H:i') . '-' . Carbon::parse($openingHour->close_at)->format('H:i');
                $hours['monday'] = [$time];
                break;
            case "Tue":
                $time = Carbon::parse($openingHour->open_at)->format('H:i') . '-' . Carbon::parse($openingHour->close_at)->format('H:i');
                $hours['tuesday'] = [$time];
                break;
            case "Wed":
                $time = Carbon::parse($openingHour->open_at)->format('H:i') . '-' . Carbon::parse($openingHour->close_at)->format('H:i');
                $hours['wednesday'] = [$time];
                break;
            case "Thu":
                $time = Carbon::parse($openingHour->open_at)->format('H:i') . '-' . Carbon::parse($openingHour->close_at)->format('H:i');
                $hours['thursday'] = [$time];
                break;
            case "Fri":
                $time = Carbon::parse($openingHour->open_at)->format('H:i') . '-' . Carbon::parse($openingHour->close_at)->format('H:i');
                $hours['friday'] = [$time];
                break;
            case "Sat":
                $time = Carbon::parse($openingHour->open_at)->format('H:i') . '-' . Carbon::parse($openingHour->close_at)->format('H:i');
                $hours['saturday'] = [$time];
                break;
            case "Sun":
                $time = Carbon::parse($openingHour->open_at)->format('H:i') . '-' . Carbon::parse($openingHour->close_at)->format('H:i');
                $hours['sunday'] = [$time];
                break;
            default:
            }
        }

        $hours = OpeningHours::create(
            $hours
        );

        // This will allow you to display things like:

        return $hours;
    }
}
