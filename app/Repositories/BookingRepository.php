<?php

namespace App\Repositories;

use App\Booking;
use App\Repositories\BaseRepository;

/**
 * Class BookingRepository
 *
 * @package App\Repositories
 * @version June 9, 2020, 12:19 am +08
 */

class BookingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
       'slot'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Booking::class;
    }

    public function betweenHours($start, $end)
    {
        return Booking::whereBetween('slot', [$start, $end])->get();
    }
}
