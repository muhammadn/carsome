<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Services\BookingsService;
use App\Services\BookingsSelection;
use App\Http\Controllers\AppBaseController;

class BookingsController extends AppBaseController
{

    // dependency injection for code easier to test;
    private $bookingsService;
    private $bookingsSelection;

    public function __construct(BookingsService $bookingsService,
        BookingsSelection $bookingsSelection
    ) {
        $this->bookingsService = $bookingsService;
        $this->bookingsSelection = $bookingsSelection;
    }

    // rest of the code to make it as slim as possible

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // run to the service layer where all the business logic resides
        return $this->bookingsService->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // run to the service layer where all the business logic resides
        $result = $this->bookingsSelection->checkSlot($request);
        // successfully created timeslot
        if (isset($result->slot))
            // guard clause return early if condition met
            return $this->sendResponse($result, 'Successfully booked timeslot');

        return $this->sendError($result->message);
    }
}
