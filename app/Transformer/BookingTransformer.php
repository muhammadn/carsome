<?php
namespace App\Transformer;

use App\Booking;
use League\Fractal;

class BookingTransformer extends Fractal\TransformerAbstract
{
    public function transform(Booking $booking)
    {
        return [
            'id'      => (int) $booking->id,
        'slot'    => $booking->slot,
                'links'   => [
                    [
                        'rel' => 'self',
                        'uri' => '/bookings/'.$booking->id,
                    ]
                ],
        ];
    }
}
