<?php

namespace Tests\Unit\Services;

use Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingsSelectionServiceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    protected $bookingsSelection;

    public function setUp(): void {
        parent::setUp();
        $this->bookingsSelection = $this->app->make('App\Services\BookingsSelection');
    }

    use RefreshDatabase;

    public function testSlotsSameHour()
    {
        $this->markTestSkipped('Requires JWT token, will fix later');
        $datetime = (object) ['slot' => '2020-06-06 09:00:00'];
	$booking = $this->bookingsSelection->checkSlot($datetime);
	$this->assertEquals('2020-06-06 09:00:00', $booking->slot->toDateTimeString());
    }

    public function testSlotsDifferentHour()
    {   
        $this->markTestSkipped('Requires JWT token, will fix later');
        $datetime = (object) ['slot' => '2020-06-06 09:00:00'];
        $booking = $this->bookingsSelection->checkSlot($datetime);
        $this->assertNotEquals('2020-06-06 12:00:00', $booking->slot->toDateTimeString());
    }

    public function testClosed()
    {
        $this->markTestSkipped('Requires JWT token, will fix later');
        $datetime = (object) ['slot' => '2020-06-06 18:00:00'];
        $booking = $this->bookingsSelection->checkSlot($datetime);
        $this->assertNotEquals('data', $booking->data);
    }

    public function testCreateBookingOnWeekdayWithMoreThan2Slots()
    {
        for($i = 0; $i < 2; $i++) {
            ${'user'.$i} = factory(\App\User::class)
                ->create(['password' => bcrypt('password')]);
            factory(\App\Booking::class)
                ->create(['user_id' => ${'user'.$i}->id, 'slot' => '2020-06-04 10:01:00']);
	}
        // third user to book third slot on weekday within 30 mins
        $datetime = Carbon::parse('2020-06-04 10:15:00');
        $booking = $this->bookingsSelection->inspectionSlots($datetime);
        $this->assertEquals(false, $booking);
    }

    public function testCreateBookingOnSaturdayWithMoreThan4Slots()
    {
        for($i = 0; $i < 4; $i++) {
            ${'user'.$i} = factory(\App\User::class)
                ->create(['password' => bcrypt('password')]);
            factory(\App\Booking::class)
                ->create(['user_id' => ${'user'.$i}->id, 'slot' => '2020-06-06 10:01:00']);
        }
        // third user to book third slot on weekday within 30 mins
        $datetime = Carbon::parse('2020-06-06 10:15:00');
        $booking = $this->bookingsSelection->inspectionSlots($datetime);
        $this->assertEquals(false, $booking);
    }

    public function testCreateBookingOnSaturdayOneSlot()
    {
        $user = factory(\App\User::class)
            ->create(['password' => bcrypt('password')]);
        factory(\App\Booking::class)
            ->create(['user_id' => $user->id, 'slot' => '2020-06-06 10:01:00']);
        // third user to book third slot on weekday within 30 mins
        $datetime = Carbon::parse('2020-06-06 10:15:00');
        $booking = $this->bookingsSelection->inspectionSlots($datetime);
        $this->assertEquals(true, $booking);
    }

    public function testCreateBookingOnWeekdayOneSlot()
    {
        $user = factory(\App\User::class)
            ->create(['password' => bcrypt('password')]);
        factory(\App\Booking::class)
            ->create(['user_id' => $user->id, 'slot' => '2020-06-04 10:01:00']);
        // third user to book third slot on weekday within 30 mins
        $datetime = Carbon::parse('2020-06-04 10:15:00');
        $booking = $this->bookingsSelection->inspectionSlots($datetime);
        $this->assertEquals(true, $booking);
    }
}
