<?php

namespace Tests\Unit\Controllers;

use Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    protected $bookingsSelection;

    public function setUp(): void {
        parent::setUp();
        $this->bookingsSelection = $this->app->make('App\Services\BookingsSelection');
    }

    use RefreshDatabase;

    public function testCreateBookingOpen()
    {
        $user = factory(\App\User::class)
	    ->create(['password' => bcrypt('password')]);

        $booking = ['slot' => '2020-06-06 09:00:00'];
	$response = $this->post('/api/v1/bookings', $booking, $this->headers($user));
	$response->assertJson([
            'message' => 'Successfully booked timeslot'
        ]);
    }

    public function testCreateBookingOpenAt0930()
    {
        $user = factory(\App\User::class)
            ->create(['password' => bcrypt('password')]);

        $booking = ['slot' => '2020-06-06 09:30:00'];
        $response = $this->post('/api/v1/bookings', $booking, $this->headers($user));
        $response->assertJson([
            'message' => 'Successfully booked timeslot'
        ]);
    }

    public function testCreateBookingOpenAt1759()
    {
        $user = factory(\App\User::class)
            ->create(['password' => bcrypt('password')]);

        $booking = ['slot' => '2020-06-06 17:59:59'];
        $response = $this->post('/api/v1/bookings', $booking, $this->headers($user));
        $response->assertJson([
            'message' => 'Successfully booked timeslot'
        ]);
    }

    public function testCreateBookingAfterOffice()
    {
        $user = factory(\App\User::class)
            ->create(['password' => bcrypt('password')]);

        $booking = ['slot' => '2020-06-06 23:00:00'];
        $response = $this->post('/api/v1/bookings', $booking, $this->headers($user));
        $response->assertJson([
            'message' => 'Carsome is not opened on this date or time'
        ]);
    }

    public function testCreateBookingOnSunday()
    {
        $user = factory(\App\User::class)
            ->create(['password' => bcrypt('password')]);

        $booking = ['slot' => '2020-06-07 10:00:00'];
        $response = $this->post('/api/v1/bookings', $booking, $this->headers($user));
        $response->assertJson([
            'message' => 'Carsome is not opened on this date or time'
        ]);
    }

    public function testCreateBookingOnSundayAndAfterOffice()
    {
        $user = factory(\App\User::class)
            ->create(['password' => bcrypt('password')]);

        $booking = ['slot' => '2020-06-07 10:00:00'];
        $response = $this->post('/api/v1/bookings', $booking, $this->headers($user));
        $response->assertJson([
            'message' => 'Carsome is not opened on this date or time'
        ]);
    }

    public function testGetBookings()
    {
        $user = factory(\App\User::class)
            ->create(['password' => bcrypt('password')]);

        $booking = \App\Booking::create(['slot' => '2020-06-06 10:00:00']);
        $response = $this->get('/api/v1/bookings', $this->headers($user));
	$response->assertJson([
	    "data" => [
            [
		'type' => 'bookings',
		'id' => (string) $booking->id,
		'attributes' => [
	            'slot' => '2020-06-06 10:00:00']
		]
	    ]
        ]);
    }

    public function testGetBookingsDuplicate()
    {
        $user = factory(\App\User::class)
            ->create(['password' => bcrypt('password')]);
	factory(\App\Booking::class)->create(['slot' => '2020-06-06 10:00:00',
	                                      'user_id' => $user->id]);

        $booking = ['slot' => '2020-06-06 11:00:00'];
        $response = $this->post('/api/v1/bookings', $booking, $this->headers($user));
        $response->assertJson([
            'message' => 'Sorry, you can only book once every 3 weeks'
        ]);
    }

    public function testCreateBookingOnSaturdayWithMoreThan4Slots()
    {
        //NOTE:  start from user0
	for($i = 0; $i < 5; $i++) {
            ${'user'.$i} = factory(\App\User::class)
                ->create(['password' => bcrypt('password')]);
        }

        for($i = 0; $i < 4; $i++) {
            factory(\App\Booking::class)
                ->create(['user_id' => ${'user'.$i}->id, 'slot' => '2020-06-06 10:01:00']);
        }

	// third user to book a slot on saturday within 30 mins
        $booking = ['slot' => '2020-06-06 10:15:00'];
        $response = $this->post('/api/v1/bookings', $booking, $this->headers($user4));
        $response->assertJson([
            'message' => 'Sorry, this timeslot is full'
        ]);
    }

    public function testCreateBookingOnWeekdayWithMoreThan2Slots()
    {
	//NOTE: start from user0
        for($i = 0; $i < 3; $i++) {
            ${'user'.$i} = factory(\App\User::class)
                ->create(['password' => bcrypt('password')]);
        }

	for($i = 0; $i < 2; $i++) {
            factory(\App\Booking::class)
                ->create(['user_id' => ${'user'.$i}->id, 'slot' => '2020-06-04 10:01:00']);
	}

        // third user to book a slot on weekday within 30 mins
        $booking = ['slot' => '2020-06-04 10:15:00'];
        $response = $this->post('/api/v1/bookings', $booking, $this->headers($user2));
        $response->assertJson([
            'message' => 'Sorry, this timeslot is full'
        ]);
    }
}
