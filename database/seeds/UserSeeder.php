<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	for($i = 0; $i < 10; $i++){
  	  User::create([
	    'name' => 'User' . $i,
	    'email' => 'user' . $i . '@example.com',
	    'password' => Hash::make('password')
          ]);
        }
    }
}
