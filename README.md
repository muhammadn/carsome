Steps:

1. `composer install`
2. `cp .env.custom .env'
3. `touch database/database.sqlite'
4. `php artisan migrate`
5. `php artisan db:seed`

Login using:

email user1@example.com to user10@example.com
password is **password**
